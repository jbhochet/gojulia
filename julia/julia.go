package julia

import "math/cmplx"

func Julia(x, y float64, c complex128, maxIter int) int {
	z := complex(x, y)
	n := 0

	for n < maxIter && cmplx.Abs(z) < 2 {
		z = cmplx.Pow(z, 2) + c
		n++
	}

	return n
}
