package image

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"os"
	"sync"

	"github.com/crazy3lf/colorconv"
	"github.com/mazznoer/colorgrad"
	"gitlab.com/jbhochet/gojulia/config"
	"gitlab.com/jbhochet/gojulia/julia"
)

type JuliaImage struct {
	height   int
	width    int
	offsetX  float64
	offsetY  float64
	zoom     float64
	maxIter  int
	complex  complex128
	gradient colorgrad.Gradient
	image    *image.RGBA
}

func New(conf *config.Config) (*JuliaImage, error) {
	// Create colors gradient
	grad, err := colorgrad.NewGradient().
		HtmlColors(conf.Colors...).
		Domain(0, float64(conf.MaxIter-1)).
		Build()
	if err != nil {
		return nil, fmt.Errorf("can't create colors gradient: %w", err)
	}

	// Create a new image height*width
	rect := image.Rect(0, 0, conf.Width, conf.Height)
	img := image.NewRGBA(rect)

	return &JuliaImage{
		conf.Height,
		conf.Width,
		conf.OffsetX,
		conf.OffsetY,
		conf.Zoom,
		conf.MaxIter,
		complex(conf.C.Real, conf.C.Imag),
		grad,
		img,
	}, nil
}

func (jmg *JuliaImage) getColor(distance int) color.RGBA {
	col := jmg.gradient.At(float64(distance)).HexString()
	r, g, b, _ := colorconv.HexToRGB(col)
	return color.RGBA{r, g, b, 255}
}

func (jmg *JuliaImage) setPixel(x, y int, color color.RGBA) {
	index := (y*jmg.width + x) * 4
	pix := jmg.image.Pix[index : index+4]
	pix[0] = color.R
	pix[1] = color.G
	pix[2] = color.B
	pix[3] = color.A
}

func (jmg *JuliaImage) buildColumn(wg *sync.WaitGroup, x int) {
	defer wg.Done()

	for y := 0; y < jmg.height; y++ {
		// Translate coordinate system's origin
		xbis := float64(x - jmg.width/2)
		ybis := float64(y - jmg.height/2)
		// Reframe
		targetHeight, targetWidth := reframe(float64(jmg.height), float64(jmg.width), 2)
		xbis = (xbis * targetWidth) / float64(jmg.width)
		ybis = (ybis * targetHeight) / float64(jmg.height)
		// Zoom
		xbis, ybis = zoom(xbis, ybis, jmg.zoom, jmg.offsetX, jmg.offsetY)
		// compute julia
		distance := julia.Julia(xbis, ybis, jmg.complex, jmg.maxIter)
		// convert distance to color
		color := jmg.getColor(distance)
		// set pixel color
		jmg.setPixel(x, y, color)
	}
}

func (jmg *JuliaImage) Build() {
	var wg sync.WaitGroup

	for x := 0; x < jmg.width; x++ {
		wg.Add(1)
		go jmg.buildColumn(&wg, x)
	}

	wg.Wait()
}

func (ji *JuliaImage) Save(filename string) error {
	file, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		return fmt.Errorf("can't open destination image file: %w", err)
	}
	defer file.Close()

	err = png.Encode(file, ji.image)
	if err != nil {
		return fmt.Errorf("failled encode image to png: %w", err)
	}

	return nil
}
