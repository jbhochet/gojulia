package image

func reframe(height, width, size float64) (float64, float64) {
	var newHeight, newWidth float64

	if min(height, width) == height {
		newHeight = size
		newWidth = (size * width) / height
	} else {
		newHeight = (size * height) / width
		newWidth = size
	}

	return newHeight, newWidth
}

func zoom(x, y, zoom, offsetX, offsetY float64) (float64, float64) {
	newX := (x + offsetX) * zoom
	newY := (y + offsetY) * zoom

	return newX, newY
}

func min(a, b float64) float64 {
	if a > b {
		return b
	} else {
		return a
	}
}
