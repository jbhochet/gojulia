module gitlab.com/jbhochet/gojulia

go 1.20

require (
	github.com/crazy3lf/colorconv v1.2.0
	github.com/mazznoer/colorgrad v0.10.0
	github.com/urfave/cli/v3 v3.0.0-beta1
)

require github.com/mazznoer/csscolorparser v0.1.5 // indirect
