package config

import (
	"encoding/json"
	"fmt"
	"os"
)

type ComplexConfig struct {
	Real float64 `json:"real"`
	Imag float64 `json:"imag"`
}

type Config struct {
	Height  int           `json:"height"`
	Width   int           `json:"width"`
	OffsetX float64       `json:"offset_x"`
	OffsetY float64       `json:"offset_y"`
	Zoom    float64       `json:"zoom"`
	MaxIter int           `json:"max_iter"`
	C       ComplexConfig `json:"complex"`
	Colors  []string      `json:"colors"`
}

func Load(filename string) (*Config, error) {
	bytes, err := os.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("can't read config file: %w", err)
	}

	conf := &Config{}

	err = json.Unmarshal(bytes, conf)
	if err != nil {
		return nil, fmt.Errorf("can't unmarshal json config: %w", err)
	}

	return conf, nil
}
