# GoJulia

A simple tool to generate pictures of the Julia set with a color palette.

## Quick start

1. Clone this repo locally.
2. Build the executable with `go build`.
3. Now you can run the generated binary!

Look at the help command (`-h`) for information on required flags!

## Configuration

You must configure the fractal from a configuration file. This is the structure
this json file:

```json
{
    "height": "THE HEIGHT OF THE IMAGE [INTEGER]",
    "width": "THE WIDTH OF THE IMAGE [INTEGER]",
    "zoom": "THE ZOOM FACTOR [FLOAT]",
    "offset_x": "THE OFFSET OF X FROM THE ORIGIN [FLOAT]",
    "offset_y": "THE OFFSET OF Y FROM THE ORIGIN [FLOAT]",
    "complex": {
        "real": "THE REAL PART OF THE COMPLEX NUMBER [FLOAT]",
        "imag": "THE IMAGINARY PART OF THE COMPLEX NUMBER [FLOAT]"
    },
    "max_iter": "THE MAXIMUN ITERATION FOR THE JULIA SERIE [INTEGER]",
    "colors": [
        "#ffffff",
        "#000000"
    ]
}
```
