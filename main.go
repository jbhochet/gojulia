package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/urfave/cli/v3"
	"gitlab.com/jbhochet/gojulia/config"
	"gitlab.com/jbhochet/gojulia/image"
)

func main() {
	cmd := &cli.Command{
		Commands: []*cli.Command{
			{
				Name:   "init",
				Usage:  "Create an empty config file",
				Action: init_command,
			},
			{
				Name:  "generate",
				Usage: "Generate a fractal image",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "config",
						Usage:    "Config file path",
						Required: true,
					},
					&cli.StringFlag{
						Name:  "output",
						Value: "image.png",
						Usage: "Output png image file path",
					},
				},
				Action: generate_command,
			},
		},
	}

	if err := cmd.Run(context.Background(), os.Args); err != nil {
		log.Fatal(err)
	}
}

func init_command(ctx context.Context, cmd *cli.Command) error {
	println("init")

	return nil
}

func generate_command(ctx context.Context, cmd *cli.Command) error {
	println("generate")

	configPath := cmd.String("config")
	outputPath := cmd.String("output")

	conf, err := config.Load(configPath)
	if err != nil {
		fmt.Println("Can't load config file")
		panic(err)
	}

	juliaImg, err := image.New(conf)

	if err != nil {
		fmt.Println("Can't init julia image")
		panic(err)
	}

	before := time.Now()

	juliaImg.Build()

	seconds := time.Since(before).Seconds()

	err = juliaImg.Save(outputPath)
	if err != nil {
		fmt.Println("Can't save image.")
		panic(err)
	}

	fmt.Printf("Fractal saved in %s (%.3fs)\n", outputPath, seconds)
	return nil
}
